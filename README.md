# JSON dereferencer

This project provides an interface and an implementation of a PHP class to replace JSON references by their targets.

## Features
For now, it only supports `$ref` paths starting with root element `#`.

## Example

Input:

```json
  {
    "responses": {
      "200": {
        "description": "The response",
        "schema": {
          "$ref": "#/components/schemas/user" 
        }
      }
    },
    "components": {
      "schemas": {
        "user": {
          "type": "string",
          "format": "email"
        }
      }
    }
  }
```

Output:

```json
  {
    "responses": {
      "200": {
        "description": "The response",
        "schema": {
          "type": "string",
          "format": "email"
        }
      }
    },
    "components": {
      "schemas": {
        "user": {
          "type": "string",
          "format": "email"
        }
      }
    }
  }
```
