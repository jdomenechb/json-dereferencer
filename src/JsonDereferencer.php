<?php

declare(strict_types=1);

/**
 * This file is part of the json-dereferencer package.
 *
 * (c) Jordi Domènech Bonilla
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Jdomenechb\JsonDereferencer;


interface JsonDereferencer
{
    /**
     * @param mixed $json
     *
     * @return mixed
     */
    public function dereference($json);
}