<?php

declare(strict_types=1);

/**
 * This file is part of the json-dereferencer package.
 *
 * (c) Jordi Domènech Bonilla
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Jdomenechb\JsonDereferencer;


class RecursiveJsonDereferencer implements JsonDereferencer
{
    /** @var mixed */
    private $json;

    /** @var array  */
    private $dereferencedRefs = [];

    /**
     * @param mixed $json
     *
     * @return mixed
     */
    public function dereference($json)
    {
        $this->json = $json;

        if (is_array($json)) {
            $json = $this->traverseJson($json);
        }

        $this->json = null;
        $this->dereferencedRefs = [];

        return $json;
    }

    /**
     * @param array $data
     *
     * @return array
     */
    private function traverseJson(array $data): array
    {
        // Treat references
        if (isset($data['$ref'])) {
            $path = $data['$ref'];

            if (isset($this->dereferencedRefs[$path])) {
                // Obtain from the cache the ref
                $resolvedReference = $this->dereferencedRefs[$path];
            } else {
                // Find target from path
                $resolvedReference = $this->findTarget($data, $path);

                // Dereference the target
                if (\is_array($resolvedReference)) {
                    $resolvedReference = $this->traverseJson($resolvedReference);
                }

                $this->dereferencedRefs[$path] = $resolvedReference;
            }

            $data = array_merge($resolvedReference, $data);
            unset($data['$ref']);

            return $data;
        }

        // Dereference all contained arrays
        foreach ($data as $key => $value) {
            if (is_array($value)) {
                $data[$key] = $this->traverseJson($value);
            }
        }

        return $data;
    }

    /**
     * @param array $data
     * @param       $path
     *
     * @return array|mixed
     */
    private function findTarget(array $data, $path)
    {
        $pathPieces = explode('/', $path);
        $resolvedReference = $data;

        foreach ($pathPieces as $pathPiece) {
            if ($pathPiece === '#') {
                // Root case
                $resolvedReference = $this->json;
            } else {
                // Relative case
                $resolvedReference = $resolvedReference[$pathPiece];
            }
        }
        return $resolvedReference;
}
}